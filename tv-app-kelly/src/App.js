import React from "react";
import { BrowserRouter, Route } from "react-router-dom";
import { Switch } from "react-router";
import NavBar from "./components/navbar";
import Homepage from "./components/homepage";
import Episode from "./components/episodePage";
import Error from "./components/error";

import "./css/app.css";

function App() {
  const stylesObj = {
    background: "#343a40"
  };
  return (
    <BrowserRouter>
      <div className="App" style={stylesObj}>
        <NavBar />
        <Switch>
          <Route exact path="/" component={Homepage} />
          <Route
            exact
            path="/series/:seriesid/episode/:season/:episodeid"
            component={Episode}
          />
          {/* if no route, then go to error page. Doesn't seem to work anymore for some reason? */}
          <Route component={Error} />
        </Switch>
      </div>
    </BrowserRouter>
  );
}

export default App;
