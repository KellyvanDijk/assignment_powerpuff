import React, { Component } from "react";
import Card from "react-bootstrap/Card";
import Button from "react-bootstrap/Button";
import { BrowserRouter as Router, Link, Route, Switch } from "react-router-dom";

class EpisodePage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      episodeIsLoaded: false,
      episodeDetails: []
    };
  }
  render() {
    if (!this.state.episodeIsLoaded) return null;
    console.log(this.state);
    return (
      <Card className="mx-auto episode-card" style={{ width: "60%" }}>
        <Card.Img
          variant="top"
          src={this.state.episodeDetails.image.original}
          className="episode-img-top"
        />
        <Card.Body>
          <Card.Title>{this.state.episodeDetails.name}</Card.Title>
          <Card.Body>
            <div
              dangerouslySetInnerHTML={{
                __html: this.state.episodeDetails.summary
              }}
            />
          </Card.Body>
          <Button className="btn-block" variant="success">
            <Link to="/" style={{ color: "#FFF" }}>
              Back
            </Link>
          </Button>
        </Card.Body>
      </Card>
    );
  }

  // I already have all the episode information fetched in seriesBlock.jsx... Should send the data I need instead of fetching again.
  componentDidMount() {
    fetch(
      "http://api.tvmaze.com/shows/" +
        this.props.match.params.seriesid +
        "/episodebynumber?season=" +
        this.props.match.params.season +
        "&number=" +
        this.props.match.params.episodeid
    )
      .then(res => res.json())
      .then(
        result => {
          this.setState({
            episodeIsLoaded: true,
            episodeDetails: result
          });
        },
        // Should add better error handling here. Don't catch it in the component
        // Should do when time left
        error => {
          this.setState({
            episodeIsLoaded: true,
            error
          });
        }
      );
  }
}

export default EpisodePage;
