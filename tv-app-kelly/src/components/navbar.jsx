import React, { Component } from "react";
import Navbar from "react-bootstrap/Navbar";
import Logo from "./logo.svg";
import Watchee from "./watchee-logo.svg";

class NavigationBar extends Component {
  render() {
    return (
      <Navbar className="fixed-top" bg="dark" variant="dark">
        <Navbar.Brand href="#home">
          <img
            alt=""
            src={Logo}
            width="30"
            height="30"
            className="d-inline-block align-top"
          />
          <img
            alt=""
            src={Watchee}
            width="auto"
            height="25"
            className="d-inline-block align-top"
          />
          {/* {" Watchee"} */}
        </Navbar.Brand>
      </Navbar>
    );
  }
}

export default NavigationBar;
