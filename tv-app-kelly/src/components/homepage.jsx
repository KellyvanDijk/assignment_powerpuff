import React, { Component } from "react";
import SeriesBlock from "./seriesBlock";
import CardDeck from "react-bootstrap/CardDeck";

class Homepage extends Component {
  state = {
    // series to render
    // Could be fetched from external file if I have time instead of hardcoding.
    series: [{ id: 6771 }, { id: 69 }, { id: 171 }]
  };
  render() {
    return (
      <div>
        <CardDeck>
          {this.state.series.map(series => (
            <SeriesBlock key={series.id} value={series.id} />
          ))}
        </CardDeck>
      </div>
    );
  }
}

export default Homepage;
