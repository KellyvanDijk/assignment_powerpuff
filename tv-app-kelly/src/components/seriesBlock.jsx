import React, { Component } from "react";
import Card from "react-bootstrap/Card";
import Button from "react-bootstrap/Button";
import Tab from "react-bootstrap/Tab";
import Tabs from "react-bootstrap/Tabs";
import Table from "react-bootstrap/Table";
//
import { BrowserRouter as Router, Link, Route, Switch } from "react-router-dom";

class SeriesBlock extends Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      resultIsLoaded: false,
      episodesAreLoaded: false,
      seriesResult: [],
      episodeList: []
    };
  }

  render() {
    if (!this.state.resultIsLoaded) return null;
    if (!this.state.episodesAreLoaded) return null;

    const episodeTable = this.state.episodeList.map(episode => (
      <tr key={episode.id}>
        <td>
          S{episode.season}E{episode.number}
        </td>
        <td>{episode.name}</td>
        <td>
          <Button variant="success">
            <Link
              to={
                "/series/" +
                this.state.seriesResult.id +
                "/episode/" +
                episode.season +
                "/" +
                episode.number
              }
              style={{ color: "#FFF" }}
            >
              Details
            </Link>
          </Button>
        </td>
      </tr>
    ));

    return (
      <Card>
        <Card.Img variant="top" src={this.state.seriesResult.image.original} />
        <Card.Body>
          <Card.Title>{this.state.seriesResult.name}</Card.Title>
          <Tabs
            defaultActiveKey={1}
            id="uncontrolled-tab-example"
            className="tab-class"
          >
            <Tab eventKey={1} title="Summary">
              <div
                dangerouslySetInnerHTML={{
                  __html: this.state.seriesResult.summary
                }}
              />
            </Tab>
            {/* Showing episodes this way is not the way I imagined it. Want some sort of pagination/divided by season. Couldn't get it to work; but this should be better */}
            <Tab eventKey={2} title="Episodes">
              <Table>
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>-></th>
                  </tr>
                </thead>
                <tbody>{episodeTable}</tbody>
              </Table>
            </Tab>
            {/* Extra Tab for information... Does not scale well to different screens + need to decide what I want to show */}
            {/* <Tab eventKey={3} title="Extra information">
              <ul>
                <li>Premiere date: {this.state.seriesResult.premiered}</li>
                <li>
                  Average rating: {this.state.seriesResult.rating.average}
                </li>
              </ul>
            </Tab> */}
          </Tabs>
        </Card.Body>
      </Card>
    );
  }

  componentDidMount() {
    fetch("http://api.tvmaze.com/shows/" + this.props.value)
      .then(res => res.json())
      .then(
        result => {
          this.setState({
            resultIsLoaded: true,
            seriesResult: result
          });
        },
        // Should add better error handling here. Don't catch it in the component
        // Should do when time left
        error => {
          this.setState({
            resultIsLoaded: true,
            error
          });
        }
      );

    //Fetch all episodes for show. Should only fetch information I need.
    // For now, fetch everything. Edit when time left.
    fetch("http://api.tvmaze.com/shows/" + this.props.value + "/episodes")
      .then(res => res.json())
      .then(
        result => {
          this.setState({
            episodesAreLoaded: true,
            episodeList: result
          });
        },
        // Should add better error handling here. Don't catch it in the component
        // Should do when time left
        error => {
          this.setState({
            episodesAreLoaded: true,
            error
          });
        }
      );
  }
}

export default SeriesBlock;
